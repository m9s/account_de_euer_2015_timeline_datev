# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'German DATEV Template Collection 2015 for EÜR',
    'name_de_DE': 'Deutsche DATEV Vorlagensammlung 2015 für EÜR',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''DATEV Template Collection 2015 for EÜR
    ''',
    'description_de_DE': '''DATEV Vorlagen Sammlung 2015 für EÜR
    ''',
    'depends': [
        'account_de_euer_2015_timeline',
        'account_timeline_tax_de_datev',
    ],
    'xml': [
        'account_de_euer_2015_datev.xml',
    ],
    'translation': [
    ],
}
